
## 0.9.0 [05-30-2023]

* Minor/adapt 2433

See merge request itentialopensource/adapters/devops-netops/adapter-gitlab!16

---

## 0.8.1 [02-02-2023]

* Added option to task and updated adapter-utils version to fix security issues

See merge request itentialopensource/adapters/devops-netops/adapter-gitlab!15

---

## 0.8.0 [05-19-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/devops-netops/adapter-gitlab!13

---

## 0.7.4 [07-19-2021]

- Fix broadcast message endpoint

See merge request itentialopensource/adapters/devops-netops/adapter-gitlab!12

---

## 0.7.3 [07-06-2021]

- If IAP version >= 2020, then exclude mongoProps, redis, and rabbitmqProps.

See merge request itentialopensource/adapters/devops-netops/adapter-gitlab!11

---

## 0.7.2 [07-06-2021]

- Updated README.md for ADAPT-733

See merge request itentialopensource/adapters/devops-netops/adapter-gitlab!10

---

## 0.7.1 [04-05-2021]

- Add per page parameter to getV4ProjectsIdRepositoryTree - this will break this call if it exists in workflows since it changes the method signature

See merge request itentialopensource/adapters/devops-netops/adapter-gitlab!9

---

## 0.7.0 [03-31-2021]

- Add 2 new calls required for prebuilt

See merge request itentialopensource/adapters/devops-netops/adapter-gitlab!8

---

## 0.6.4 [03-07-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/devops-netops/adapter-gitlab!7

---

## 0.6.3 [07-08-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/devops-netops/adapter-gitlab!6

---

## 0.6.2 [01-29-2020]

- Update this in the schema file, there are probably more changes like this needed but would need to know everything that we wanted to change before changing them.

See merge request itentialopensource/adapters/devops-netops/adapter-gitlab!5

---

## 0.6.1 [01-09-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/devops-netops/adapter-gitlab!4

---

## 0.6.0 [11-07-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/devops-netops/adapter-gitlab!3

---

## 0.5.0 [09-17-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/devops-netops/adapter-gitlab!2

---
## 0.4.0 [07-30-2019] & 0.3.0 [07-18-2019]

- Migrate to the latest adapter foundation, categorization and prepare for app artifact

See merge request itentialopensource/adapters/devops-netops/adapter-gitlab!1

---

## 0.2.1 [07-08-2019] & 0.1.1 [06-17-2019]

- Initial Commits

See commit 0a4424e

---
