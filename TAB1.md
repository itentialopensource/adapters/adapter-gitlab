# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the GitLab System. The API that was used to build the adapter for GitLab is usually available in the report directory of this adapter. The adapter utilizes the GitLab API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The GitLab adapter from Itential is used to integrate the Itential Automation Platform (IAP) with GitLab. With this adapter you have the ability to perform operations such as:

- Get a GitLab project's information
- Commit a new change to a GitLab repository 
- Create or manage a merge request in a GitLab repository

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
